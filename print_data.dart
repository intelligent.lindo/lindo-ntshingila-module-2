import 'dart:io'; 

void main() {
  //Prompt the user for information
  print("Hello! Allow us to get to know you!");
  print("Please enter your name: ");
  String? name = stdin.readLineSync();
  print("What's your favourite app?");
  String? favApp = stdin.readLineSync();
  print("What city are you in?");
  String? city = stdin.readLineSync();

  print("Hi team! My name is $name.");
  print("I'm based in $city.");
  print("My favourite app is $favApp.");
}