void main() {
  //Create arrays for the winners
  List<String> arr2021Winners = ['Ambani Afrika', 'Takealot', 'Shyft', 'iiDENTIFii', 'Sisa', 'Guardian Health Platform', 'Murimi', 'UniWise', 'Kazi-App', 'Rekindle', 'Hellopay SoftPOS', 'Roadsave'];
  List<String> arr2020Winners = ['Checkers Sixty60', 'Easy Equities', 'Birdpro', 'Technishen', 'Matric Live', 'Stokfella', 'Bottles', 'Lexie Hearing', 'League of Legends', 'Examsta', 'xiTsonga Dictionary', 'GreenFingers Mobile', 'Guardian Health', 'My Pregnancy Journey'];
  List<String> arr2019Winners = ['Digger', 'SI Realities', 'Vula Mobile', 'Hydra Farm', 'Matric Live', 'Franc', 'Over', 'Loctransi', 'Naked Insurance', 'My Pregnancy Journey', 'Loot Defense', 'Mowash'];
  List<String> arr2018Winners = ['Cowa-bunga', 'ACGI', 'Besmarta', 'Xander English', 'Ctrl', 'Pineapple', 'Bestee', 'Stokfella', 'ASI'];
  List<String> arr2017Winners = ['OrderIn', 'InterGreatMe', 'Pick n Pays Super Animals 2', 'WatIf Health Portal', 'TransUnion 1Check', 'Hey Jude', 'Oru Social Touch SA', 'Awethu Project', 'Zulzi', 'Shyft', 'EcoSlips'];
  List<String> arr2016Winners = ['Domestly', 'Friendly Math Monsters for Kindergarten', 'HearZA', 'iKhokha', 'Kaching', 'Mibrand', 'Tuta-me'];
  List<String> arr2015Winners = ['CPUT Mobile', 'DStv Now', 'Eskom sePush', 'M4Jam', 'Vula Mobile', 'Wumdrop'];
  List<String> arr2014Winners = ['Live Inspect', 'My Belongings', 'ReaVaya', 'SuperSport', 'Sync Mobile', 'Vigo', 'Wildlife Tracker', 'Zapper'];
  List<String> arr2013Winners = ['Bookly', 'Deloitte Digital', 'DStv', 'Gautrain Buddy', 'Kids Aid', 'MarkIt Share', 'Nedbank', 'PriceCheck', 'SnapScan'];
  List<String> arr2012Winners = ['FNB Banking', 'HealthID', 'Phrazapp', 'Plascon', 'Rapid Targets', 'Transunion Dealers Guide'];
  
  //Combining arrays and sorting it into alphabetic order
  List<String> newArrWinners = arr2012Winners + arr2013Winners + arr2014Winners + arr2015Winners + arr2016Winners + arr2017Winners + arr2018Winners + arr2019Winners + arr2020Winners + arr2021Winners;
  newArrWinners.sort();
  
  //Print the new array onto the terminal
  print(newArrWinners);
  print(' ');

  //Print the winning apps by calling the arrays
  print('The winning apps in 2017 are: $arr2017Winners');
  print(' ');
  print('The winning apps in 2018 are: $arr2018Winners');
  print(' ');

  //Create a new variable for the total number of winners
  var totalWinners = newArrWinners.length;
  print('The total number of apps is: $totalWinners');
}