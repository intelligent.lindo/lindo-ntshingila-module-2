class Apps {
  late String name;
  late String category;
  late String developer;
  late int year;

  //Uppercase function
  String allCap() {
    var all_caps = name.toUpperCase();
    print(all_caps);
    return all_caps;
  }

}

void main() {
  Apps Ambani = Apps();
  Ambani.name = 'Ambani Africa';
  Ambani.category = 'Best Gaming Solution';
  Ambani.developer = 'Ambani';
  Ambani.year = 2021;

  print(Ambani);
  print('The name of the winning app is: ${Ambani.name}');
  print('The app won an award for: ${Ambani.category}');
  print('The developer of the app is: ${Ambani.developer}');
  print('The app won in the year: ${Ambani.year}');

  Ambani.allCap();
}